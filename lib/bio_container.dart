import 'package:flutter/material.dart';

class BioContainer extends StatelessWidget {
  final Color color;
  final double height;
  final Alignment alignment;
  final String text;
  final double fontSize;
  final double radius;

  BioContainer({
    this.color = Colors.white,
    this.height = 70,
    this.alignment = Alignment.center,
    required this.text,
    this.fontSize = 20,
    this.radius = 10,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom:10,
        top: 10,
        right: 10,
        left: 10,
      ),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(radius),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(1),
            offset: Offset(0, 1),
            blurRadius: 3,
          )
        ],
      ),
      height: height,
      alignment: alignment,
      child: Text(
        text,
        // textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: fontSize,
        ),
      ),
    );
  }
}
