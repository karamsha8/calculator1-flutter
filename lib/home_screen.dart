import 'package:calclutor2/bio_container.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        // margin: EdgeInsets.only(bottom: 10),
        child: Column(
         mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                     '0.0',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ),
              ],
            ),
            Row(
             mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: BioContainer(
                      text: 'CE',
                      color: Colors.orange,
                    ),
                ),
                Expanded(
                  child: BioContainer(
                    text: 'C',
                    color: Colors.orange,

                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '%',
                    color: Colors.orange,
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '+',
                    color: Colors.blue,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [

                Expanded(
                  child: BioContainer(
                    text: '7',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '8',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '9',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '*',
                    color: Colors.teal,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: BioContainer(
                    text: '3',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '4',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '5',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '-',
                    color: Colors.green,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [

                Expanded(
                  child: BioContainer(
                    text: '1',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '2',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '3',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '+',
                    color: Colors.deepPurpleAccent,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [

                Expanded(
                  child: BioContainer(
                    text: '+/-',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '0',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '',
                  ),
                ),
                Expanded(
                  child: BioContainer(
                    text: '=',
                    color: Colors.green,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
